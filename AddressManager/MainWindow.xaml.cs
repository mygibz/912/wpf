﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AddressManager {
	public class Adress {
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Street { get; set; }
		public string Zip { get; set; }
		public string City { get; set; }
	}
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		string path = "";
		public MainWindow() {
			InitializeComponent();
			//Address_listView.Items.Add(new Adress { FirstName = "Tyer", LastName = "Lee" });

		}
		private void Add_Button_Click(object sender, RoutedEventArgs e) {
			// add input to list
			Address_listView.Items.Add(new Adress { 
				FirstName = Firstname_TextBox.Text, 
				LastName = Lastname_TextBox.Text,
				Street = Address_TextBox.Text,
				Zip = PLZ_TextBox.Text,
				City = Ort_TextBox.Text
			});
			clearForms();
		}

		private void Open_MenuItem_Click(object sender, RoutedEventArgs e) {

		}

		private void Save_MenuItem_Click(object sender, RoutedEventArgs e) {
			writeToFile(false);
		}

		private void Save_As_MenuItem_Click(object sender, RoutedEventArgs e) {
			writeToFile(true);
		}

		private void Quit_MenuItem_Click(object sender, RoutedEventArgs e) {
			this.Close();
		}

		private void Help_MenuItem_Click(object sender, RoutedEventArgs e) {
			System.Windows.MessageBox.Show("Help yourself");
		}

		private void Search_Button_Click(object sender, RoutedEventArgs e) {

		}

		public void writeToFile(bool saveAs) {
			// saves a file
			if (path == "" || saveAs == true) {
				SaveFileDialog saveFileDialog = new SaveFileDialog();
				saveFileDialog.Filter = "text-files (.txt)|*.txt|markdown-files (.md)|*.md|all|*.*";
				saveFileDialog.ShowDialog();
				path = saveFileDialog.FileName;
			}
			try {
				using (StreamWriter sw = new StreamWriter(path)) {
					//sw.WriteLine(Address_listView.s);
				}
			}
			catch {
				System.Windows.MessageBox.Show("Invalid File path");
			}
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e) {
			// disabled Add_Button until there's text in every input-field
			if (Firstname_TextBox.Text != "" && Lastname_TextBox.Text != "" && Address_TextBox.Text != "" && PLZ_TextBox.Text != "" && Ort_TextBox.Text != "") {
				Add_Button.IsEnabled = true;
			} else {
				Add_Button.IsEnabled = false;
			}
		}

		private void clearForms() {
			Firstname_TextBox.Text = "";
			Lastname_TextBox.Text = "";
			Address_TextBox.Text = "";
			PLZ_TextBox.Text = "";
			Ort_TextBox.Text = "";
		}
	}
}
