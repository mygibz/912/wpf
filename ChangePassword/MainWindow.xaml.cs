﻿using System.Windows;

namespace ChangePassword {
    /// 26.11.2019, Yanik Ammann
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }


        private void Input_Old_Password_Text_Box_PasswordChanged(object sender, RoutedEventArgs e) {
            // check if password is correct enable the other input fields
            if (Input_Old_Password_Text_Box.Password == "K90bbg6") {
                Input_New_Password_One_Text_Box.IsEnabled = true;
                Input_New_Password_Two_Text_Box.IsEnabled = true;
            } else {
                Input_New_Password_One_Text_Box.IsEnabled = false;
                Input_New_Password_Two_Text_Box.IsEnabled = false;
            }
        }

        private void Change_Password_Button_Click(object sender, RoutedEventArgs e) {
            bool goodEnough = true;
            string password = Input_New_Password_Two_Text_Box.Password;
            string errorMessage = "";

            // min length
            if (password.Length < 6) {
                goodEnough = false;
                errorMessage += "The password needs to be at least 6 digits long!\n";
            }

            // is number in it
            bool numberFound = false;
            foreach (char item in password) {
                string d = item.ToString();
                // könnte auch mit Convert.ToInt32() und try/catch gemacht werden
                if (d == "0" || d == "1" || d == "2" || d == "3" || d == "4" || d == "5" || d == "6" || d == "7" || d == "8" || d == "9")
                    numberFound = true;
            }
            if (numberFound == false) { 
                goodEnough = false;
                errorMessage += "The password needs to be contain at least one number!\n";
            }

            // min one upper case letter
            bool foundCapitalized = false;
            foreach (char item in password) {
                if (item.ToString().ToUpper() == item.ToString())
                    foundCapitalized = true;
            }
            if (foundCapitalized == false) {
                goodEnough = false;
                errorMessage += "The password needs to be contain at least one upper-case letter!\n";
            }

            // min one lower case letter
            bool foundLowerCapitalized = false;
            foreach (char item in password) {
                if (item.ToString().ToLower() == item.ToString())
                    foundLowerCapitalized = true;
            }
            if (foundLowerCapitalized == false) {
                goodEnough = false;
                errorMessage += "The password needs to be contain at least one lower-case letter!\n";
            } 

            // give output
            if (goodEnough == true) {
                MessageBox.Show("Passwort erfolgreich geändert!", "Erfolg", MessageBoxButton.OK, MessageBoxImage.Information);
            } else {
                MessageBox.Show("Password couldnt be changed, it has to follow these requirements:\n" + errorMessage);
            }
        }

        private void Input_New_Password_One_Text_Box_PasswordChanged(object sender, RoutedEventArgs e) {
            // check if password is correct enable the other input fields
            if (Input_New_Password_One_Text_Box.Password == Input_New_Password_Two_Text_Box.Password) {
                Change_Password_Button.IsEnabled = true;
            } else {
                Change_Password_Button.IsEnabled = false;
            }
        }
    }
}
