﻿using System.Windows;

namespace Schachbrettgenerator {
    /// 26.11.2019, Yanik Ammann
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        string[] letters = { "A", "B", "C", "D", "E", "F", "G", "H" };

        private void Create_Board_Button_Click(object sender, RoutedEventArgs e) {
            Create_Board_Button.IsEnabled = false;
            Delete_Board_Button.IsEnabled = true;

            string output = "";
            // generate Board
            for (int i = 1; i <= 8; i++) {
                for (int j = 0; j < 8; j++) {
                    output += letters[j] + i + "   ";
                }
                output += "\n";
            }
            Output_Text_Block.Text = output;
        }

        private void Delete_Board_Button_Click(object sender, RoutedEventArgs e) {
            Delete_Board_Button.IsEnabled = false;
            Create_Board_Button.IsEnabled = true;

            Output_Text_Block.Text = "";
        }
    }
}
