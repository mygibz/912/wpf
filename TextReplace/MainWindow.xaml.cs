﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TextReplace {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void Submit_Button_Click(object sender, RoutedEventArgs e) {
            // read variables
            string inputFullString = Input_Text_Text_Box.Text;
            int position = Convert.ToInt32(Input_Position_Text_Box.Text) - 1;
            // do magic
            string beginString = inputFullString.Substring(0, position);
            string endString = inputFullString.Substring(position, (inputFullString.Length - position));
            // output
            Input_Text_Text_Box.Text = beginString + Input_Replace_Text_Box.Text + endString;
        }
    }
}
