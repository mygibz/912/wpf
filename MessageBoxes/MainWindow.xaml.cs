﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MessageBoxes {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		public MainWindow() {
			InitializeComponent();
		}

		private void Button_Click_1(object sender, RoutedEventArgs e) {
			MessageBox.Show("Something happened", "Hello gays", MessageBoxButton.YesNo, MessageBoxImage.Asterisk);
		}

		private void Button_Click_2(object sender, RoutedEventArgs e) {
			MessageBoxResult MsgBox = MessageBox.Show("Yes or no...?", "Morning dear traveler", MessageBoxButton.YesNo, MessageBoxImage.Question);
			if (MsgBox.ToString() == "Yes") {
				MessageBox.Show("actually.... No!");
			} else {
				MessageBox.Show("actually.... Yes!");
			}
		}

		private void Button_Click_3(object sender, RoutedEventArgs e) {
			MessageBox.Show("You're an idiot", "It's true", MessageBoxButton.YesNoCancel, MessageBoxImage.Hand);
		}

		private void Button_Click_4(object sender, RoutedEventArgs e) {
			if (MessageBox.Show("Do you want to enter the cave of ultimate gayness", "Morning dear explorer", MessageBoxButton.YesNo, MessageBoxImage.Question).ToString() == "Yes") {
				if (MessageBox.Show("There's a monster visible, fight it?", "The Cave", MessageBoxButton.YesNo, MessageBoxImage.Question).ToString() == "Yes") {
					MessageBox.Show("You thought well... but the monster was too strong\nYou died!", "The End", MessageBoxButton.OK, MessageBoxImage.Stop);
				} else {
					MessageBox.Show("You tried to flee... but the monster was faster than you\nYou died!", "The End", MessageBoxButton.OK, MessageBoxImage.Stop);
				}
			} else {
				MessageBox.Show("You go home and enjoy a nice evening with your family", "The End");
			}
		}
	}
}
