﻿using System.Windows;

namespace SearchAndReplace {
    /// 25.11.2019, Yanik Ammann
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void Submit_Button_Click(object sender, RoutedEventArgs e) {
            string input = Input_Text_Text_Box.Text;
            // check if found
            if (input.IndexOf(Input_Search_Text_Box.Text) != -1) {
                // replace
                Input_Text_Text_Box.Text = input.Replace(Input_Search_Text_Box.Text, Input_Replace_Text_Box.Text);
            } else {
                // whoopsie
                MessageBox.Show("Der Text \'" + Input_Search_Text_Box.Text + "\' wurde nicht gefunden.");
            }
        }
    }
}
