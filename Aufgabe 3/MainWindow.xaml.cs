﻿// 29.10.2019, Yanik Ammann, prints out selected inputs

using System.Windows;

namespace Aufgabe_3 {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		public MainWindow() {
			InitializeComponent();
		}

		private void show_button_Click(object sender, RoutedEventArgs e) {
			// create variables
			int checkedOfOptions1 = 1;
			int checkedOfOptions2 = 1;
			string checked31 = " ";
			string checked32 = " ";
			string checked33 = " ";

			// check checked
			if (option11.IsChecked == true) {
				checkedOfOptions1 = 1;
			} else if (option12.IsChecked == true) {
				checkedOfOptions1 = 2;
			} else if (option13.IsChecked == true) {
				checkedOfOptions1 = 3;
			}

			if (option21.IsChecked == true) {
				checkedOfOptions2 = 1;
			} else if (option22.IsChecked == true) {
				checkedOfOptions2 = 2;
			} else if (option23.IsChecked == true) {
				checkedOfOptions2 = 3;
			}

			if (option31.IsChecked == true) {
				checked31 = "\nCheckBoyOption3.1 is checked";
			}
			if (option32.IsChecked == true) {
				checked32 = "\nCheckBoyOption3.2 is checked";
			}
			if (option33.IsChecked == true) {
				checked33 = "\nCheckBoyOption3.3 is checked";
			}


			// output
			MessageBox.Show("Options 1\nRadioButton option 1." + checkedOfOptions1 + " is checked\n\nOptions 2\nRadioButton Option 2." + checkedOfOptions2 + 
				" is checked\n\nOptions 3" + checked31 + checked32 + checked33);
		}
	}
}
