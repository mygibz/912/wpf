﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Calculate_Fakultät {
    // 03.12.2019, Yanik Ammann
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void Input_Text_Box_TextChanged(object sender, TextChangedEventArgs e) {
            try {
                // initiate variables
                double input = Convert.ToDouble(Input_Text_Box.Text);
                int ammountOfRuns = Convert.ToInt32(input);
                double output = 1;

                // do calculations
                for (int i = 1; i <= ammountOfRuns; i++) {
                    output *= i;
                }

                // output
                Output_Text_Box.Text = output.ToString();
            }
            catch {

            }
        }
    }
}
