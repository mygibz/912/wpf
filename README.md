## AddressManager
An Application to add and remove contacts to an AdressBoox, supports saving those contact books into files too

## AlterBerechnen
Allows the user to calculate the seconds, days, weeks, month and years since a given birthday

### Aufgabe 1 - Hello World
Hello World toggle switch

### Aufgabe 2 - List
Allows the user to add elements to a list

### Aufgabe 3
Simple Checkbox and Radio button to Message Box converter

### Aufgabe 4
You can move food from a Combobox to a Listview and back again using buttons.... yay!!!

### Aufgabe 5 - Image viewer
Allows you to load an image

### Aufgabe 6 - Countdown
Counts down from 10, you can input your own time ammount in seconds

> Does not actually count one second per second because... it just does

## Aufgabe 7 - Christmas Tree generator
Allows the user to generate a custom sized christmas tree

### Aufgabe 8 - Simple Text Editor
You can edit text-based, it also supports loading and saving those files to and from disk

### Aufgabe 9 - Simple Bank Account
Allows you to Deposit and Withdraw money to a virtual Bank Account

## Aufgabe Shoppinglist
The user can add and remove items from Coop, Migros and Denner to a virtual Shoppinglist.
The application then shows the combined value (total cost) of the items and can generate a *receipt* from it

### Calculate Fakultät
Calculates the faculty of a given number

### ChangePassword
It's a change-password-dialog... it changes passwords...

### CountVocals
Counts the ammount of Vocals in a given text

## Drashow
You can select a folder and it opens the images inside it and allows you to go through them, it even has limited animations

### Extract Filename
Extracts the Filename from a Windows-FilePath

### FIFO-Puffer
> A FIFO buffer is a useful way of storing data that arrives to a microcontroller peripheral asynchronously but cannot be read immediately. One example is storing bytes incoming on a UART. Buffering the bytes eases the real-time requirements for the embedded firmware.

### Generate Quadrat
Generates a (text) square

### IntToBinary
Converts an interger to binary

### Mehrfach-Eingaben
Counts the count of 10 numbers tat you can input (only supports the numbers 0-9)

## MessageBoxes
A simple Message Box Adventure

### morsecode
Generates Morsecode from a text

## MP3-Player
Allows you to play a MP3-File

### Output Text Backwards
Outputs an input text backwards, also allows to invert the capitalisation of that text

### Queersumme
Calculates the cross sum of a given number

### RemoveHTMLAnnotations
Removes HTML tags from an input

## Roman Number
Converts a number to the roman number system and back

### Schachbrettgenerator
Generates a chess-board

### Schraubensack
It was a task, don't judge

### SearchAndReplace
Allows the user to replace text with other text in text

### SortArray
Allows the user to sort an array (sample input: `1 5 3 2 4` (only supports single digit numbers))

### Stoppuhr
Allows the user to count the time (not working)

### TextReplace
Allows the user to insert text at a position

### URL validator
Tells the user if a certain Email or URL is valid or not

## Vieleck
Connects dots together with lines

## Waldbrand
Simulates a Forest Fire

### Word Counter
Counts the ammount of words in a text

### WpfApp1
Login form demo

### XHTML Validieren
Checks if input (text) has a valid XML/HTML/XHTML syntax

## Zahlenraten
Game where you guess a number (between 1 and 100) and shows you how many tries you required to get to that number