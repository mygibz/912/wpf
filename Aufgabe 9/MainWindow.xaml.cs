﻿// 06.11.2019, Yanik Ammann, add or remove an ammount from account

using System;
using System.Windows;

namespace Aufgabe_9 {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {

		// only here because for some reason we need to use classes
		public class Account {
			// define global variables
			public double ammountInDeposit;

			public void RemoveFromAccount(string x) {
				// removes money from account
				if (Convert.ToDouble(x) < 0) {
					MessageBox.Show("Cannot Add negative ammounts");
				} else {
					try {
						// check if there's enough money in account
						if (ammountInDeposit >= Convert.ToDouble(x)) {
							ammountInDeposit -= Convert.ToDouble(x);
						} else {
							// not enough money in account
							MessageBox.Show("You don't have enough money to do that");
						}
					}
					catch {

					}
				}
			}
			public void AddToAccount(string x) {
				// check if entered number more than 0
				try {
					if (Convert.ToDouble(x) < 0) {
						// a negative number has been entered
						MessageBox.Show("Cannot Add negative ammounts");
					} else {
						// adds money to account
						ammountInDeposit += Convert.ToDouble(x);
					}
				}
				catch {

				}
			}
		}
		Account account = new Account();
		

		public MainWindow() {
			InitializeComponent();
		}

		private void Withdraw_Button_Click(object sender, RoutedEventArgs e) {
			// withdraws money from account (remove)
			account.RemoveFromAccount(Amount_Text_Box.Text);
			updateDisplay();
		}

		private void Deposit_Button_Click(object sender, RoutedEventArgs e) {
			// deposit money to account (add)
			account.AddToAccount(Amount_Text_Box.Text);
			updateDisplay();
		}

		public void updateDisplay() {
			// sets the account-ammount label to ammountInDeposit
			Ammount.Text = account.ammountInDeposit.ToString();
		}
	}

	
}
