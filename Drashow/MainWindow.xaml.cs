﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media.Imaging;

namespace Drashow {
    // 09.12.2019, Yanik Ammann
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        string[] files;
        int selectedImage = 0;

        private void Open_Button_Click(object sender, RoutedEventArgs e) {
            using (var fbd = new FolderBrowserDialog()) {
                DialogResult result = fbd.ShowDialog();
                // save filenames in folder to array
                if (!string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    files = Directory.GetFiles(fbd.SelectedPath, "*.??g");
            }
            Image_Frame.Source = new BitmapImage(new Uri(files[selectedImage]));
        }

        private void Right_Button_Click(object sender, RoutedEventArgs e) {
            // go one image to the right
            if (selectedImage != files.Length - 1)
                selectedImage++;
            else
                selectedImage = 0;
            showImage();
        }
        private void Left_Button_Click(object sender, RoutedEventArgs e) {
            // go one image to the left
            if (selectedImage != 0)
                selectedImage--;
            else
                selectedImage = files.Length - 1;
            showImage();
        }


       async private void showImage() {
            // create task
            Task<bool> opacityMinus = OpacityMinus();
            // wait for task
            bool success1 = await opacityMinus;

            // converts selected file to bitmap and displays it
            Image_Frame.Source = new BitmapImage(new Uri(files[selectedImage]));

            // create task
            Task<bool> opacityPlus = OpacityPlus();
            // wait for task
            bool success2 = await opacityPlus;
        }

        public async Task<bool> OpacityMinus() {
            // remove opacity
            for (Image_Frame.Opacity = 1; Image_Frame.Opacity < 0; Image_Frame.Opacity -= 0.05)
                await Task.Delay(20);
            return true;
        }

        public async Task<bool> OpacityPlus() {
            // add opacity
            for (Image_Frame.Opacity = 0; Image_Frame.Opacity < 1; Image_Frame.Opacity += 0.05)
                await Task.Delay(20);
            return true;
        }
    }
}
