﻿using System;
using System.Windows;
using System.Windows.Threading;
using System.Diagnostics;

namespace Stoppuhr {
    // 04.12.2019, Yanik Ammann
    public partial class MainWindow : Window {

        public MainWindow() {
            InitializeComponent();
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += updateView;
            dispatcherTimer.Interval = TimeSpan.FromMilliseconds(10);
        }

        Stopwatch stopWatch = new Stopwatch();
        DateTime lastRound;
        DispatcherTimer dispatcherTimer;
        int running = 0;
        int rundencounter = 0;

        private void Start_Button_Click(object sender, RoutedEventArgs e) {
            if (running == 0) { //start
                dispatcherTimer.Start();
                Start_Button.Content = "Stoppen";
                Round_Button.Content = "Runde";
                running = 1;
                Round_Button.IsEnabled = true;
                lastRound = DateTime.Now;
                stopWatch.Start();
            } else if (running == 1) { //pause
                dispatcherTimer.Stop();
                Start_Button.Content = "Weiter";
                Round_Button.Content = "Löschen";
                running = 2;
                stopWatch.Stop();
            } else if (running == 2) { //delete
                dispatcherTimer.Start();
                Start_Button.Content = "Stoppen";
                Round_Button.Content = "Runde";
                running = 1;
                stopWatch.Start();
            }
        }

        private void Round_Button_Click(object sender, RoutedEventArgs e) {
            if (running == 1) {
                rundencounter++;
                Rounds_List_View.Items.Add("Runde " + rundencounter.ToString() + "\t\t\t\t" + Convert.ToString(DateTime.Now - lastRound).Substring(3, 8).Replace(".", ":"));
                lastRound = DateTime.Now;
            } else {
                // reset everything
                running = 0;
                Start_Button.Content = "Starten";
                Round_Button.Content = "Runde";
                Rounds_List_View.Items.Clear();
                Counter_Text_Box.Text = "00:00:00";
                rundencounter = 0;
                stopWatch.Reset();
            }
        }

        private void updateView(object sender, EventArgs e) {
            Counter_Text_Box.Text = Convert.ToString(stopWatch.Elapsed).Substring(3, 8).Replace(".", ":");
        }


        //public MainWindow() {
        //    InitializeComponent();
        //    dispatcherTimer = new DispatcherTimer();
        //    dispatcherTimer.Tick += updateView;
        //    dispatcherTimer.Interval = TimeSpan.FromMilliseconds(10);
        //}

        //DateTime startTime;
        //DateTime PauseTime;
        //DateTime lastRound;
        //DispatcherTimer dispatcherTimer;
        //int running = 0;
        //int rundencounter = 0;

        //private void Start_Button_Click(object sender, RoutedEventArgs e) {
        //    if (running == 0) { //start
        //        startTime = DateTime.Now;
        //        dispatcherTimer.Start();
        //        Start_Button.Content = "Stoppen";
        //        Round_Button.Content = "Runde";
        //        running = 1;
        //        Round_Button.IsEnabled = true;
        //        lastRound = DateTime.Now;
        //    } else if (running == 1) { //pause
        //        dispatcherTimer.Stop();
        //        Start_Button.Content = "Weiter";
        //        Round_Button.Content = "Löschen";
        //        running = 2;
        //        // remember pause time
        //        PauseTime = DateTime.Now;
        //    } else if (running == 2) { //resume
        //        dispatcherTimer.Start();
        //        Start_Button.Content = "Stoppen";
        //        Round_Button.Content = "Runde";
        //        running = 1;
        //        try {
        //            // recall pause time
        //            int hour = startTime.Hour + (DateTime.Now.Hour - PauseTime.Hour);
        //            int minute = startTime.Minute + (DateTime.Now.Minute - PauseTime.Minute);
        //            int second = startTime.Second + (DateTime.Now.Second - PauseTime.Second);
        //            int milliseconds = startTime.Millisecond + (DateTime.Now.Millisecond - PauseTime.Millisecond);
        //            startTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, hour, minute, second, milliseconds);
        //        }
        //        catch {

        //        }

        //    }
        //}

        //private void Round_Button_Click(object sender, RoutedEventArgs e) {
        //    if (running == 1) {
        //        rundencounter++;
        //        Rounds_List_View.Items.Add("Runde " + rundencounter.ToString() + "\t\t\t\t" + Convert.ToString(DateTime.Now - lastRound).Substring(3, 8).Replace(".", ":"));
        //        lastRound = DateTime.Now;
        //    } else {
        //        running = 0;
        //        Start_Button.Content = "Starten";
        //        Round_Button.Content = "Runde";
        //        Rounds_List_View.Items.Clear();
        //        Counter_Text_Box.Text = "00:00:00";
        //        rundencounter = 0;
        //    }
        //}

        //private void updateView(object sender, EventArgs e) {
        //    Counter_Text_Box.Text = Convert.ToString(DateTime.Now - startTime).Substring(3, 8).Replace(".", ":");
        //}
    }
}
