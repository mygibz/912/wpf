﻿// 04.11.2019, Yanik Ammann, generates a Tannenbaum using a custom Class

using System;
using System.Windows;
using System.Windows.Controls;

namespace Aufgabe_7 {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		Tanne t = new Tanne();
		public MainWindow() {
			InitializeComponent();
		}

		public class Tanne {
			// initiate variables for object
			public int CrownHeight;
			public int TrunkWidth;
			public int TrunkHeight;
			public string Result;
			public void Draw() {
				Result = "";

				#region generate Crown
				for (int i = 0; i < CrownHeight; i++) {
					// every line
					for (int x = 0; x <= i * 2; x++) {
						Result += "*";
					}
					Result += "\n";
				}
				#endregion

				#region generate Trunk
				for (int i = 0; i < TrunkHeight; i++) {
					// every line
					for (int x = 0; x < TrunkWidth; x++) {
						Result += "*";
					}
					Result += "\n";
				}
				#endregion
			}
		}

		private void TextChanged(object sender, TextChangedEventArgs e) {
			// generates Tree
			try {
				// inputs variables and starts draw
				t.CrownHeight = Convert.ToInt32(tbHeight.Text);
				t.TrunkWidth = Convert.ToInt32(tbTrunkWidth.Text);
				t.TrunkHeight = Convert.ToInt32(tbTrunkHeight.Text);
				t.Draw();
				tbOutput.Text = t.Result;
			}
			catch {

			}
		}
	}
}
