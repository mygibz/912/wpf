﻿// 30.10.2019, Yanik Ammann, allows to view an image that is choosen throuch a FileDialog

using System;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media.Imaging;

namespace Aufgabe_5 {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		string initialPathSelection = "c:\\";
		public MainWindow() {
			InitializeComponent();
		}

		private void load_image_button_Click(object sender, RoutedEventArgs e) {
			//
			// allows the user to select a file and displays it as a bitmap image
			//
			OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.InitialDirectory = initialPathSelection;
			openFileDialog.Filter = "png-image (.png)|*.png|jpg-image (.jpg)|*.jpg";
			openFileDialog.ShowDialog();
			try {
				// converts selected file to bitmap and displays it
				image_frame.Source = new BitmapImage(new Uri(openFileDialog.FileName));
				// set initialPath to selected folder
				initialPathSelection = openFileDialog.FileName;
			}
			catch (Exception) {
				Console.WriteLine("ERROR: no file selected");
			}
		}
	}
}
