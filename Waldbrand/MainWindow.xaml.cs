﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace Waldbrand {
    /// 04.12.2019, Yanik Ammann
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += simulateForestTick;
            dispatcherTimer.Interval = TimeSpan.FromMilliseconds(250);
        }

        // set variables
        int[,] forest;
        int forestWidth;
        int forestDepth;
        int growthPercentage;
        int fireSpreadingPercentage;

        // set usables
        Random random = new Random();
        DispatcherTimer dispatcherTimer;

        private string GetString(int i) {
            if (Input_Use_Icons_Check_Box.IsChecked == true) {
                string[] objectsLibrary = { "-", "🌲", "🎄", "🔥", "🧱" };
                return objectsLibrary[i];
            } else {
                string[] objectsLibrary = { "-", "B", "F", "F", "S" };
                return objectsLibrary[i];
            }
        }

        private void Submit_Button_Click(object sender, RoutedEventArgs e) {
            try {
                // get inputs
                forestWidth = Convert.ToInt32(Input_Width_Forest_TextBox.Text);
                forestDepth = Convert.ToInt32(Input_Depth_Forest_TextBox.Text);
                growthPercentage = Convert.ToInt32(Input_Growth_Percentage_TextBox.Text);
                fireSpreadingPercentage = Convert.ToInt32(Input_Fire_Spreading_Percentage_TextBox.Text);
                if (growthPercentage > 100 || fireSpreadingPercentage > 100)
                    MessageBox.Show("An Error happened processing your inputs...\nPercentages over 100% are not supported");
                forest = new int[forestDepth, forestWidth];

                // disable all input
                Input_Width_Forest_TextBox.IsEnabled = false;
                Input_Depth_Forest_TextBox.IsEnabled = false;
                Input_Growth_Percentage_TextBox.IsEnabled = false; ;
                Input_Fire_Spreading_Percentage_TextBox.IsEnabled = false;
                Input_Use_Icons_Check_Box.IsEnabled = false;
                Submit_Button.IsEnabled = false;
            }
            catch { 
                MessageBox.Show("An Error happened processing your inputs...\n" +
                    "Did you type something wrong?");
            }

            // generate forest
            for (int i = 0; i < forestDepth; i++)
                for (int j = 0; j < forestWidth; j++) {
                    int randomNumber = random.Next(0, 9);
                    if (randomNumber == 4)
                        forest[i, j] = 1; // tree
                    else if (randomNumber == 2)
                        forest[i, j] = 4; // stone
                    else
                        forest[i, j] = 0; // floor (nothing)
                }

            printForest();
            Submit_Button.IsEnabled = false;
            dispatcherTimer.Start();
        }

        private void printForest() {
            string output = "";
            for (int i = 0; i < forestDepth; i++) { // row
                for (int j = 0; j < forestWidth; j++)  // column
                    output += GetString(Convert.ToInt32(forest[i, j]));
                output += "\n";
            }
            Output_TextBox.Text = output;
        }

        private void simulateForestTick(object sender, EventArgs e) {
            for (int i = 0; i < forestDepth; i++) // you burned enough
                for (int j = 0; j < forestWidth; j++)
                    if (forest[i, j] == 3)
                        forest[i, j] = 0;

            for (int i = 0; i < forestDepth; i++) // you're still burning
                for (int j = 0; j < forestWidth; j++)
                    if (forest[i, j] == 2)
                        forest[i, j] = 3;

            int randomGrowthNumber = random.Next(0, 100 - growthPercentage); // grow
            for (int i = 0; i < forestDepth; i++)
                for (int j = 0; j < forestWidth; j++)
                    if (forest[i, j] == 0)
                        if (random.Next(0, 100 - growthPercentage) == randomGrowthNumber)
                            forest[i, j] = 1;

            int randomBurnNumber = random.Next(0, 100 - fireSpreadingPercentage); // burn
            for (int i = 0; i < forestDepth; i++)
                for (int j = 0; j < forestWidth; j++)
                    if (this.forest[i, j] == 1)
                        if (random.Next(0, 100 - fireSpreadingPercentage) == randomBurnNumber)
                            this.forest[i, j] = 2;
           
            for (int i = 0; i < forestDepth; i++) // definitely burn everything DOWN!!!
                for (int j = 0; j < forestWidth; j++) {
                    try {
                        if (forest[i, j] == 3) {
                            if (forest[i + 1, j] == 1)
                                forest[i + 1, j] = 2;
                            if (forest[i, j + 1] == 1)
                                forest[i, j + 1] = 2;
                            if (forest[i - 1, j] == 1)
                                forest[i - 1, j] = 2;
                            if (forest[i, j - 1] == 1)
                                forest[i, j - 1] = 2;
                        }
                    }
                    catch { }
                }

            printForest();
        }
    }
}
