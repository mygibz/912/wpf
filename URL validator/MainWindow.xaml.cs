﻿using System.Windows;
using System.Text.RegularExpressions;

namespace URL_validator {
    /// 26.11.2019, Yanik Ammann
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void Check_Email_Button_Click(object sender, RoutedEventArgs e) {
            string input = Input_Email_Text_Box.Text;
            try {
                // replace
                Regex myRegex = new Regex(@"^([A-Z|a-z|0-9](\.){0,1})+[A-Z|a-z|0-9]\@([A-Z|a-z|0-9])+((\.){0,1}[A-Z|a-z|0-9]){2}\.[a-z]{2,3}$");
                if (myRegex.Replace(input, "") != "")
                    MessageBox.Show("Email not valid");
            }
            catch {
                MessageBox.Show("whoopsie");
            }
        }

        private void Check_Url_Button_Click(object sender, RoutedEventArgs e) {
            string input = Input_URL_Text_Box.Text;
            try {
                // replace
                Regex ipRegex = new Regex(@"^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}");
                Regex domainRegex = new Regex(@"^((http:\/\/){0,1}|(https:\/\/){0,1})([A-Z|a-z|0-9]{1,127}\.){0,127}[A-Z|a-z|0-9]{0,127}\.{0,1}[A-Z|a-z|0-9]{2}\.[A-Z|a-z]{2,3}");
                if (ipRegex.Replace(input, "") != "" && domainRegex.Replace(input, "") != "")
                    MessageBox.Show("URL not valid");
            }
            catch {
                MessageBox.Show("whoopsie");
            }
        }
    }
}
